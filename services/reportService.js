const reportModel = require('../models/reportModel.js');

const createData = (bodyData, callback) => {
    reportModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}
const findOneData = (bodyData, options={}, callback) => {
    reportModel.findOne(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
const getData = (bodyData, options={}, callback) => {
    reportModel.find(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// update only one record
const updateOneData = (query, bodyData, options, callback) => {
    reportModel.update(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// find only one record async
const findOneAsync = async (query, options = {}) => {
    return await reportModel.findOne(query, options).lean();
}

// Delete record
const deleteRecord = (query, callback) => {
    reportModel.deleteOne(query, (err, result) => {
        callback(err, result);
    });
}

const populateData = (query, callback) => {
    reportModel.find(query)
    .populate({path:"reported_id",select:"username dob profile_image country bio hobbies blocks"})
    .exec((err, result) => {
         callback(err, result);
    });
}


module.exports = {
    createData,
    findOneData,
    getData,
    updateOneData,
    findOneAsync,
    deleteRecord,
    populateData
};