const contentModel = require('../models/contentModel.js');

const createData = (bodyData, callback) => {
    contentModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}
const findOneData = (bodyData, options={}, callback) => {
    contentModel.findOne(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
const getData = (bodyData, options={}, callback) => {
    contentModel.find(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// update only one record
const updateOneData = (query, bodyData, options, callback) => {
    contentModel.update(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// find only one record async
const findOneAsync = async (query, options = {}) => {
    return await contentModel.findOne(query, options).lean();
}

// find only one record async
const findAsync = async (query, options = {}) => {
    return await contentModel.find(query, options).lean();
}

// Delete record
const deleteRecord = (query, callback) => {
    contentModel.deleteOne(query, (err, result) => {
        callback(err, result);
    });
}


module.exports = {
    createData,
    findOneData,
    getData,
    updateOneData,
    findOneAsync,
    deleteRecord,
    findAsync
};