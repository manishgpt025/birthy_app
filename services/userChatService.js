const userChatModel = require('../models/userChatModel.js');

const createData = (bodyData, callback) => {
    userChatModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}
const findOneData = (bodyData, options={}, callback) => {
    userChatModel.findOne(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
const getData = (bodyData, options={}, callback) => {
    userChatModel.find(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// update only one record
const updateOneData = (query, bodyData, options, callback) => {
    userChatModel.update(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// find only one record async
const findOneAsync = async (query, options = {}) => {
    return await userChatModel.findOne(query, options).lean();
}

// Delete record
const deleteRecord = (query, callback) => {
    userChatModel.deleteOne(query, (err, result) => {
        callback(err, result);
    });
}
// Delete record
const deleteManyRecord = (query, callback) => {
    userChatModel.deleteMany(query, (err, result) => {
        callback(err, result);
    });
}


module.exports = {
    createData,
    findOneData,
    getData,
    updateOneData,
    findOneAsync,
    deleteRecord,
    deleteManyRecord
};