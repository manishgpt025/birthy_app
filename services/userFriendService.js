const userFriendModel = require('../models/userFriendModel.js');

const createData = (bodyData, callback) => {
    userFriendModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}
const findOneData = (bodyData, options={}, callback) => {
    userFriendModel.findOne(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
const getData = (bodyData, options={}, callback) => {
    userFriendModel.find(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// update only one record
const updateOneData = (query, bodyData, options, callback) => {
    userFriendModel.update(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// find only one record async
const findOneAsync = async (query, options = {}) => {
    return await userFriendModel.findOne(query, options).lean();
}

// Delete record
const deleteRecord = (query, callback) => {
    userFriendModel.deleteOne(query, (err, result) => {
        callback(err, result);
    });
}

const populateData = (query, callback) => {
    userFriendModel.find(query)
    .populate({path:"request_by",select:"username dob profile_image country bio hobbies"})
    .exec((err, result) => {
         callback(err, result);
    });
}

// Get chat list user data using aggregate
const getChatListData = (bodyData, callback) => {
    userFriendModel.aggregate([
        {
            "$match": {
                "$and":[
                    {"$or": [{request_by: bodyData.userId},{request_to: bodyData.userId}]},
                    {status:true}
                ]
            }
        },
        { "$lookup": {
            "from": "userchats",
            "as": "chat_data",
            "let": { "id": "$_id" },
            "pipeline": [
                { "$match":
                    {
                        "$expr": { "$eq": [ "$$id", "$chat_id" ] }
                    }
                },
                { "$sort":
                    {
                        "createdAt": -1
                    }
                },

               { "$limit": 1 }
            ]
          }
        },

        {
            "$addFields": {
                "chater_id": {
                  "$cond": {
                    "if": {
                      "$eq": [ "$request_by", bodyData.userId]
                    },
                    "then": "$request_to",
                    "else": "$request_by"
                  }
                }
            }
        },
        {
            $lookup: {
                from: "users",
                localField: "chater_id",
                foreignField: "_id",
                as: "chater_data"
            }
        },
        {
            "$project":{
                "chat_data":1,
                "chater_data":1
            }
        }

    ]).exec((err, result) => {
        console.log('qqqqqqqqqq',result)
        callback(err, result);
    });
};

// Get chat list user data using aggregate
const getFriendListData = (bodyData, callback) => {
    userFriendModel.aggregate([
        {
            "$match": {
                "$and":[
                    {"$or": [{request_by: bodyData.userId},{request_to: bodyData.userId}]},
                    {status:true}
                ]
            }
        },
        {
            "$addFields": {
                "chater_id": {
                  "$cond": {
                    "if": {
                      "$eq": [ "$request_by", bodyData.userId]
                    },
                    "then": "$request_to",
                    "else": "$request_by"
                  }
                }
            }
        },
        {
            $lookup: {
                from: "users",
                localField: "chater_id",
                foreignField: "_id",
                as: "user_data"
            }
        },
        {
            "$project":{
                "user_data":1
            }
        }

    ]).exec((err, result) => {
        callback(err, result);
    });
};

module.exports = {
    createData,
    findOneData,
    getData,
    updateOneData,
    findOneAsync,
    deleteRecord,
    populateData,
    getChatListData,
    getFriendListData
};