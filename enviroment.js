module.exports = {
  development: {
    node_port: process.env.PORT,
    database : process.env.DATABASE_NAME,
    username : process.env.username,
    password : process.env.PASSWORD,
    db_host : process.env.DB_HOST,
    url: process.env.SERVICE_HOST
  }
};
