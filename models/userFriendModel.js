const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;

let userFriendModel = new schema({
    request_by: {
        type: schema.Types.ObjectId,
        ref:"user"
    },
    request_to: {
        type: schema.Types.ObjectId,
        ref:"user"
    },
    status: {
        type: Boolean, //0 for requested, 1 for accepted
        default: 0
    },
    chat_status: {
        type: Boolean, //1 for active, 0 for deactive
        default: 1
    },
    clear_chat_id: {
        type: schema.Types.ObjectId,
        ref:"user"
    },
},{
    timestamps: true
});

userFriendModel.plugin(mongoosePaginate)
userFriendModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('userFriend', userFriendModel);