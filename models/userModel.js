const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;

let userModel = new schema({
    username: {
        type: String,// username of person
        trim: true,
    },
    social_id: {
        type: String,//facebook login id of person
        trim: true,
        default: ""
    },
    password: {
        type: String ,// password of person
        default: ""
    },
    dob: {
        type: Date, // DOB of person
        default: ""
    },
    profile_image: {
        type: String, // Image of person
        default: ""
    },
    bio: {
        type: String,// Bio of person
        default: ""
    },
    hobbies: {
        type: String, //Hobbies of person
        default: ""
    },
    country: {
        type: String, //country name
        default: ""
    },
    lat: {
        type: String, //country latitude
        default: ""
    },
    lang: {
        type: String, //country langitude
        default: ""
    },
    status: {
        type: Boolean, //User status
        default: 1
    },
    blocks: {
        type: Array //User blocked by
    }
},{
    timestamps: true
});

userModel.plugin(mongoosePaginate)
userModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('user', userModel);