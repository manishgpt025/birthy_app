const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;

let reportModel = new schema({
    reported_id: {
        type: schema.Types.ObjectId,
        ref:"user"
    },
    report_message: {
        type: String,
        default: ""
    }
},{
    timestamps: true
});

reportModel.plugin(mongoosePaginate)
reportModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('report', reportModel);