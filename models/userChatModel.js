const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;

let userChatModel = new schema({
    chat_id: {
        type: schema.Types.ObjectId,
        ref:"userFriend"
    },
    chat_by: {
        type: schema.Types.ObjectId,
        ref:"user"
    },
    message:{
        type:String,
        default:""
    },
    status: {
        type: Boolean, //0 for active //1 for deactive
        default: 0
    },
    read:{  // 0 not read ,1 for read
        type:Boolean,
        default:0
    }
},{
    timestamps: true
});

userChatModel.plugin(mongoosePaginate)
userChatModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('userChat', userChatModel);