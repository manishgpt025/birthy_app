const dotenv = require('dotenv');
dotenv.config();
const express = require('express')
const app = express();
require('dotenv').config();
const config = require('./config.js');
bodyParser = require('body-parser');
const dbConnect = require('./mongodb');
const server = require('http').createServer(app)
cors = require('cors');
const route = require('./routes/userRoute.js');

app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }));
app.use(bodyParser.json({limit: '100mb', extended: true}));
app.use(cors());

app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);
app.use(express.static(__dirname+'/public'));

app.get('/privacy', function (req, res)
{res.render('Privacy_privacy.html')});

app.get('/terms', function (req, res)
{res.render('Terms_&_Conditions .html')});

//call route by api
app.use('/api', route);

//connectivity forn listen PORT
server.listen(process.env.PORT, () => {
    console.log('Website service app listening on port', global.gConfig.node_port)
});