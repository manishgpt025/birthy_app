var randomstring = require("randomstring");
module.exports = {
    //Check post key validation
    checkRequest: (array, obj) => {
        for (i of array) {
            if (obj[i] == undefined || obj[i] == "")
                return i;
        }
        return true;
    },
    //generate Random String
    generateRandomString: () => {
        let newId = randomstring.generate({
            length: 12,
            charset: 'hex'
        });
        return newId = newId + new Date().getTime().toString();
    },
}