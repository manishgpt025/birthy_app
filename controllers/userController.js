const userService = require('../services/userService.js');// user services
const userFriendService = require('../services/userFriendService.js');// user Friend services
const userChatService = require('../services/userChatService.js');// user content services
const contentService = require('../services/contentService.js');// user report services
const reportService = require('../services/reportService.js');// user Friend Chat services
const helper = require('../globalFunctions/function.js');// helper and glocal functions
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync();
const ObjectId = require('mongodb').ObjectID;
// JWT
const jwt = require('jsonwebtoken');
const secret =`${global.gConfig.secret}`;
const multiparty = require('multiparty');
const cloudinary = require('cloudinary');
const NodeGeocoder = require('node-geocoder');
cloudinary.config({
    cloud_name: "ddlvvchsy",
    api_key: "537114565878512",
    api_secret: "laEzPoh0vlmPPRw1A94IDXNFUcY"
});
const options = {
  provider: 'google',
  //fetch: customFetchImplementation,
  apiKey: 'AIzaSyDulJXZo8Aqi7cjejAG8_1s1SoNO-3nOsg',
  formatter: null
};

const geocoder = NodeGeocoder(options);
// Date and time
let date = require('date-and-time');
const moment = require('moment');
/**
 * [Login API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const login = async (req, res) => {
    let check = helper.checkRequest(["username", "password"], req.body);
    if (check == true) {
        userService.findOneData({ username: req.body.username.toLowerCase()},{}, (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                bcrypt.compare(req.body.password, userInfo.password, async (errorPassword, result) => {
                    if (result) {
                        let query = {
                            "_id":userInfo._id
                        };
                        let userData = await userService.findOneAsync(query,{password:0});
                        // JWT
                        let token = jwt.sign({ username: userData.username},
                            secret, {
                                expiresIn: '24h' // expires in 24 hours
                            }
                        );
                        userData.token = token;
                       return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Login successfully', userData);
                    } else {
                        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Credentials not match');
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Credentials not match');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [SignUp API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const signup = async (req, res) => {
    let form = new multiparty.Form();
    form.parse(req, (error, fields, files) => {
        if (error)
            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
        else{
            if(
                !fields.username
                    ||
                !fields.password
                    ||
                !fields.dob
                    ||
                !fields.country
            )return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'All mandatory fields are required');
            else{
                userService.findOneData({ username: fields.username[0].toLowerCase()},{}, async (errorCheck, userInfo) => {
                    if (errorCheck)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else if (userInfo)
                        return responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'This username is already used');
                    else{
                        let password = bcrypt.hashSync(fields.password[0], salt);
                        let insertData = {
                            "username": fields.username[0].toLowerCase(),
                            "password": password,
                            "dob": fields.dob ? new Date(fields.dob[0]) : "",
                            "country":fields.country ? fields.country[0] : "",
                            "profile_image":fields.profile_image ? fields.profile_image[0] : ""
                        };
                        userService.createData(insertData, async (createError, createSuccess) => {
                            if (createError)
                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                            else{
                                getLatLang(createSuccess);
                                // JWT
                                let token = jwt.sign({ username: createSuccess.username},
                                    secret, {
                                        expiresIn: '24h' // expires in 24 hours
                                    }
                                );
                                let query = {
                                    "_id":createSuccess._id
                                };
                                let userData = await userService.findOneAsync(query,{password:0});
                                userData.token = token;
                                return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'SignUp successfully', userData);
                            }
                        });
                    }
                });
            }
        }
    });
};


/**
 * [SignUp API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const upload_image = async (req, res) => {
    let form = new multiparty.Form();
    form.parse(req, (error, fields, files) => {
        if (error)
            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
        else{
            if(files && files.image && files.image[0].originalFilename){
                let pic = files.image[0].path;
                cloudinary.v2.uploader.upload(pic, {
                    resource_type: "image"
                }, (errorUpload, successUpload) => {
                    if(errorUpload)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else{
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Uploaded successfully', successUpload.secure_url);
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Image not found');
            }
        }
    });
};

/**
 * [User Image Upload]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const userImageUpload = (req, res) => {
    let check = helper.checkRequest(["image"], req.body);
    if (check == true) {
       // let buf = Buffer.from(req.body.image.replace(/^data:image\/\w+;base64,/, ""), 'base64');
        var uploadStr = 'data:image/jpeg;base64,' + req.body.image;
        cloudinary.v2.uploader.upload(uploadStr, {
            resource_type: "image"
        }, (errorUpload, successUpload) => {
            if(errorUpload)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else{
                return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Uploaded successfully', successUpload.secure_url);
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Social SignUp API form facebook]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const social_login = async (req, res) => {
    let check = helper.checkRequest(["social_id", "first_name"], req.body);
    if (check == true) {
         userService.findOneData({ social_id: req.body.social_id},{}, async (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                let query = {
                    "_id":userInfo._id
                };
                let userData = await userService.findOneAsync(query,{password:0});
                let token = jwt.sign({ username: userData.username},
                    secret, {
                        expiresIn: '24h' // expires in 24 hours
                    }
                );
                if(userData.country && userData.dob)
                    userData.is_complete = true;
                else
                    userData.is_complete = false;
                userData.token = token;
                return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Social login successfully', userData);
            }else{
                let socialId = req.body.social_id;
                let name = req.body.first_name.toLowerCase();
                let username = name + socialId.substring(socialId.length - 4)
                let insertData = {
                    "username": username,
                    "social_id": socialId,
                    "profile_image":req.body.profile_image || ""
                };
                userService.createData(insertData, async (createError, userInfo) => {
                    if (createError)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else{
                        let query = {
                            "_id":userInfo._id
                        };
                        let userData = await userService.findOneAsync(query,{password:0});
                        let token = jwt.sign({ username: userData.username},
                            secret, {
                                expiresIn: '24h' // expires in 24 hours
                            }
                        );
                        if(userData.country && userData.dob)
                            userData.is_complete = true;
                        else
                            userData.is_complete = false;
                        userData.token = token;
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Social login successfully', userData);
                    }
                });
            }
        });
    }else{
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Social SignUp complete API form facebook]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const social_complete = async (req, res) => {
    let check = helper.checkRequest(["user_id", "country","dob"], req.body);
    if (check == true) {
         userService.findOneData({_id: req.body.user_id},{}, async (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                let updateData = {
                    "country":req.body.country,
                    "dob": new Date(req.body.dob)
                };
                let query = {
                    _id:userInfo._id
                };
                userService.updateOneData(query,updateData,{ upsert: true }, async (updateError, updateSuccess) => {
                    if (updateError)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else{
                        let userData = await userService.findOneAsync(query,{password:0});
                        getLatLang(userData);
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'login successfully', userData);
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    }else{
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Get lat lang data country wise]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const get_latLang = async (req, res) => {
    userService.getLatLangData( async (error, success) => {
        if (error)
            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
        else{
            let contentData = await contentService.findAsync({},{});
            let data = {
                contentData,
                latLangData : success
            };
            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Data fetch successfully', data);
        }
    });
};

/**
 * [Change password for user]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const change_password = async (req, res) => {
   let check = helper.checkRequest(["old_password", "new_password","user_id"], req.body);
    if (check == true) {
         userService.findOneData({_id: req.body.user_id},{}, async (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                bcrypt.compare(req.body.old_password, userInfo.password, async (errorPassword, successPassword) => {
                    if (successPassword) {
                        let password = bcrypt.hashSync(req.body.new_password, salt);
                        let updateData = {
                            "passowrd":password,
                        };
                        let query = {
                            _id:userInfo._id
                        };
                        userService.updateOneData(query,updateData,{ upsert: true }, async (updateError, updateSuccess) => {
                            if (updateError)
                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                            else{
                                return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Password changed successfully');
                            }
                        });
                    } else {
                        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Old password not matched');
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    }else{
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Delete user account]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const delete_account = async (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.user_id},{},  (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                let query = {
                    "_id":userInfo._id
                };
                userService.deleteRecord(query, (errorDelete, successdelete) => {
                    if(errorDelete)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Your account deleted successfully');
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [update profile API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const update_profile = async (req, res) => {
    let form = new multiparty.Form();
    form.parse(req, (error, fields, files) => {
        if (error)
            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
        else{
            if(
                !fields.user_id
                    ||
                !fields.username
                    ||
                !fields.dob
                    ||
                !fields.country
            )return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'All mandatory fields are required');
            else{
                let query = {
                    username: fields.username[0].toLowerCase(),
                    _id: {$ne : fields.user_id[0]}
                }
                userService.findOneData(query,{}, async (errorCheck, userInfo) => {
                    if (errorCheck)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else if (userInfo)
                        return responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'This username is already used');
                    else{
                        let userQuery = {
                            "_id":fields.user_id[0]
                        };
                        let userProfileData = await userService.findOneAsync(userQuery,{password:0});
                        let newdate = "";
                        if(fields.dob && fields.dob[0])
                            newdate = date.split("-").reverse().join("-"); 
                        let updateData = {
                            "username": fields.username[0].toLowerCase(),
                            "dob": newdate,
                            "country":fields.country ? fields.country[0] : "",
                            "bio" : fields.bio ? fields.bio[0] : "",
                            "hobbies" : fields.hobbies ? fields.hobbies[0] : "",
                            "profile_image":fields.profile_image ? fields.profile_image[0] : ""
                        };
                        userService.updateOneData(userQuery, updateData,{ upsert: true }, async (updateError, updateSuccess) => {
                            if (updateError)
                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                            else{
                                let userData = await userService.findOneAsync(userQuery,{password:0});
                                if(userProfileData.country !== fields.country[0]){
                                    getLatLang(userData);
                                }
                                return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Profile updated successfully', userData);
                            }
                        });
                    }
                });
            }
        }
    });
};

/**
 * [user list API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const home_user_list = async (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.user_id},{},  (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                // let query = {
                //     _id: {$ne : userInfo._id},
                //     dob : userInfo.dob
                // }
                // if(req.body.page_number){
                //     req.body.page_number = Number.parseInt(req.body.page_number);
                // }
                // let options = {
                //     page: req.body.page_number || 1, 
                //     limit: 10,
                //     sort: { createdAt: -1 },
                // }
                let userDobMonth = userInfo.dob.getMonth()+1 > 9 ? userInfo.dob.getMonth()+1 :'0'+userInfo.dob.getMonth()+1;
                let userDobDate = userInfo.dob.getDate() > 9 ? userInfo.dob.getDate() :'0'+userInfo.dob.getDate(); 
                let dobDate =userDobMonth + '-'+userDobDate;
                let bodyData = {
                    user_id: userInfo._id,
                    dob : dobDate
                };
                if(req.body.page_number){
                    req.body.page_number = Number.parseInt(req.body.page_number);
                }
                let options = {
                    page: req.body.page_number || 1,
                    limit: req.body.limit || 10,
                }
                let sort = { "createdAt": -1 }
                userService.getEqualDobDate(bodyData, options,sort, (errorData,successData,pages,total) => {
                //userService.getPaginateData(query,options,(error,success) => {
                    if(error)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else{
                       // if(successData.length){
                            let data = {
                                docs: successData,
                                page: req.body.page_number || 1,
                                limit: req.body.limit || 10,
                                pages: pages,
                                total: total
                            }
                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'List fetch successfully',data);
                        //}else
                          //  return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'List not found');
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [new 100 user list API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const new_user_list = async (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.user_id},{},  (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                let query = {
                    _id: {$ne : userInfo._id}
                }
                if(req.body.page_number){
                    req.body.page_number = Number.parseInt(req.body.page_number);
                }
                let options = {
                    page: req.body.page_number || 1,
                    limit: 10,
                    sort: { createdAt: -1 },
                   // limit: 100
                }
                userService.getPaginateLimitData(query,options,(error,success) => {
                    if(error)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'List fetch successfully',success);
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Get profile detail API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const profile_detail = async (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.user_id},{},  (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                delete userInfo.password;
                return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Profile detail fetch successfully',userInfo);
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Dob calender event API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const dob_calender_event = async (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.user_id},{},  (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if(userInfo) {
                userService.getAllDobDate((errorDob, successDob) => {
                if (errorDob)
                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                else{
                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Dob dates fetch successfully',successDob);
                }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [date user list API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const date_user_list = async (req, res) => {
    let check = helper.checkRequest(["user_id","dob"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.user_id},{},  (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                let bodyData = {
                    user_id: userInfo._id,
                    dob : req.body.dob
                };
                if(req.body.page_number){
                    req.body.page_number = Number.parseInt(req.body.page_number);
                }
                let options = {
                    page: req.body.page_number || 1,
                    limit: req.body.limit || 10,
                }
                let sort = { "createdAt": -1 };
                userService.getEqualDobDate(bodyData, options,sort, (errorData,successData,pages,total) => {
                    if(errorData)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else{
                        if(successData.length){
                            let data = {
                                docs: successData,
                                page: req.body.page_number || 1,
                                limit: req.body.limit || 10,
                                pages: pages,
                                total: total
                            }
                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'List fetch successfully',data);
                        }else
                            return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'List not found');
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Send request API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const send_request = async (req, res) => {
    let check = helper.checkRequest(["request_by","request_to"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.request_by},{},  async(errorByData, requestByData) => {
            if (errorByData)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (requestByData) {
                let query = {
                    "_id":req.body.request_to
                };
                let requestToData = await userService.findOneAsync(query,{password:0});
                if(requestToData){
                    let queryData = {
                        $or: [
                          { $and: [{request_by: req.body.request_by}, {request_to: req.body.request_to}] },
                          { $and: [{request_by: req.body.request_to}, {request_to: req.body.request_by}] }
                        ]
                    };
                    let checkData = await userFriendService.findOneAsync(queryData,{});
                    if(checkData){
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Request send successfully');
                    }else{
                        let insertData = {
                            'request_by':requestByData._id,
                            'request_to':requestToData._id
                        };
                        userFriendService.createData(insertData, (errorInsert, successInsert) => {
                        if (errorInsert)
                            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                        else
                           return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Request send successfully');
                        });
                    }
                }else{
                    return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Requested user not found');
                }
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Send request check API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const check_request = async (req, res) => {
    let check = helper.checkRequest(["request_by","request_to"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.request_by},{},  async(errorByData, requestByData) => {
            if (errorByData)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (requestByData) {
                let query = {
                    "_id":req.body.request_to
                };
                let requestToData = await userService.findOneAsync(query,{password:0});
                if(requestToData){
                    let queryData = {
                        $or: [
                          { $and: [{request_by: req.body.request_by}, {request_to: req.body.request_to}] },
                          { $and: [{request_by: req.body.request_to}, {request_to: req.body.request_by}] }
                        ]
                    };
                    let checkData = await userFriendService.findOneAsync(queryData,{});
                    if(checkData){
                        if(checkData.status){
                            return responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'This user already in your friend list');
                        }else{
                            if(checkData.request_by == req.body.request_by){
                                return responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'You are already send request');
                            }else{
                                return responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'This user already send you request');
                            }
                        }
                    }else{
                      return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Ready for send request');
                    }
                }else{
                    return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'This user not found');
                }
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Send request response forn reciever API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const request_response = async (req, res) => {
    let check = helper.checkRequest(["resopense_by","request_from","status"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.resopense_by},{},  async(errorByData, responseByData) => {
            if (errorByData)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (responseByData) {
                let query = {
                    "_id":req.body.request_from
                };
                let requestFromData = await userService.findOneAsync(query,{password:0});
                console.log(requestFromData)
                if(requestFromData){
                    let queryData = {
                        "request_by":req.body.request_from,
                        "request_to":req.body.resopense_by
                    };
                    let checkData = await userFriendService.findOneAsync(queryData,{});
                    if(checkData){
                        if(!checkData.status){
                            if(req.body.status == "1"){
                               let updateData = {
                                    "status":1
                                };
                                userFriendService.updateOneData(queryData,updateData,{upsert:true}, (errorUpdate, successUpdate) =>{
                                    if (errorUpdate)
                                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                                    else{
                                        // let insertChat = {
                                        //     "chat_id":checkData._id,
                                        //     "chat_by":checkData.request_by,
                                        //     "read":1,
                                        // };
                                        // userChatService.createData(insertChat, (errorCreate, successCreate) =>{
                                        //     if (errorCreate)
                                        //         return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                                        //     else
                                        //         return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Request accepted successfully');
                                        // });
                                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Request accepted successfully');
                                    }
                                });
                            }else{
                               userFriendService.deleteRecord(queryData, (errorDelete, successDelete) =>{
                                    if (errorDelete)
                                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                                    else
                                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Request cancel successfully');
                                });
                            }
                        }else{
                            return responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'You are already accepted request');
                        }

                    }else{
                        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Something went wrong');
                    }
                }else{
                    return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Requested user not found');
                }
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Requested user list API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const request_user_list = async (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.user_id},{},  async(error, result) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (result) {
                let query = {
                    "request_to":req.body.user_id,
                    "status": 0
                };
                userFriendService.populateData(query , (errorData, successData) => {
                    if (errorData)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else if(successData.length){
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Requested list found', successData);
                    }else{
                        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Requested list not found');
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};


/**
 * [user friend list API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const friend_list_old = async (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        let userId = req.body.user_id;
        userService.findOneData({ _id: userId},{},  async(errorUserData, requestUserData) => {
            if (errorUserData)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (requestUserData) {
                let queryData = {
                    $and: [
                      { $or: [{request_by: userId}, {request_to: userId}] },
                      { status:1 }
                    ]
                };
                userFriendService.getData(queryData,{},  async (errorData, findListData) => {
                    if (errorData)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else{
                        if(findListData.length){
                            let friendLUserIds = [];
                            await findListData.map( async userData => {
                                if(userData.request_by == userId){
                                    friendLUserIds.push(userData.request_to);
                                }else{
                                    friendLUserIds.push(userData.request_by);
                                }
                            });
                            if(friendLUserIds.length){
                                let queryFriend = {
                                    "_id" : {
                                        "$in" :friendLUserIds
                                    }
                                }
                                let friendListUserData = await userService.findAsync(queryFriend,{password:0});
                                return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Friend list found',friendListUserData);
                            }else{
                              return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Friend list not found');
                            }
                        }else{
                          return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Friend list not found');
                        }
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [user friend list API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const friend_list = async (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        let userId = req.body.user_id;
        userService.findOneData({ _id: userId},{},  async(errorUser, successUser) => {
            if (errorUser)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (successUser) {
                let bodyData = {
                    "userId" : new ObjectId(userId)
                }
                userFriendService.getFriendListData(bodyData, async (errorData, findListData) => {
                    if (errorData)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else{
                        if(findListData.length){
                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Chat list fetch successfully',findListData);
                        }else{
                           return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Chat list not found');
                        }
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    }else{
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Send message API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const send_message = async (req, res) => {
    let check = helper.checkRequest(["sender_id","chat_id","message"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.sender_id},{},  async(errorSender, successSender) => {
            if (errorSender)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (successSender) {
                let messageData = {
                    "chat_id":req.body.chat_id,
                    "chat_by":req.body.sender_id,
                    "message":req.body.message,
                };
                userChatService.createData(messageData, async(errorInsert, successInsert) =>{
                    if (errorSender)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else{
                        let queryData = {_id : new ObjectId(req.body.chat_id)}
                        let checkData = await userFriendService.findOneAsync(queryData,{});
                        if(checkData && !checkData.chat_status){
                            let updateData = {
                            "chat_status":1
                            };
                            userFriendService.updateOneData(queryData,updateData,{ upsert: true }, async (updateError, updateSuccess) => {
                                if (errorSender)
                                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                                else{
                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Send message successfully');
                                }
                            });
                        }else{
                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Send message successfully');
                        }
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Sender not found');
            }
        });
    }else{
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [chat list API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const chat_list = async (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        let userId = req.body.user_id;
        userService.findOneData({ _id: userId},{},  async(errorUser, successUser) => {
            if (errorUser)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (successUser) {
               let queryData = {
                    $and: [
                      { $or: [{request_by: userId}, {request_to: userId}] },
                      { status:1 }
                    ]
                };
                let bodyData = {
                    "userId" : new ObjectId(userId)
                }
                userFriendService.getChatListData(bodyData, async (errorData, findListData) => {
                    if (errorData)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else{
                        if(findListData.length){
                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Chat list fetch successfully',findListData);
                        }else{
                           return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Chat list not found');
                        }
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    }else{
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [chat conversation API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const chat_conversation = async (req, res) => {
    let check = helper.checkRequest(["user_id","other_id"], req.body);
    if (check == true) {
        let userId = req.body.user_id;
        let otherId = req.body.other_id;
        userService.findOneData({ _id: userId},{},  async(errorUser, successUser) => {
            if (errorUser)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (successUser) {
                userService.findOneData({ _id: userId},{},  async(errorOther, successOther) => {
                    if (errorOther)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else if (successOther) {
                        let queryData = {
                            $or: [
                              { $and: [{request_by: userId}, {request_to: otherId}] },
                              { $and: [{request_by: otherId}, {request_to: userId}] }
                            ]
                    };
                    let checkData = await userFriendService.findOneAsync(queryData,{});
                    if(checkData && checkData.chat_status && userId != checkData.clear_chat_id){
                        let conversationId = checkData._id;
                        let query={
                            chat_id : new ObjectId(conversationId),
                            status : 0
                        };
                        let options = {message:1,chat_id:1,chat_by:1,createdAt:1,read:1}
                        userChatService.getData(query ,options , (errorData, successData) =>{
                            if (errorOther)
                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                            else{
                                if(successData.length)
                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'conversation found', successData);
                                else
                                    return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'conversation not found');
                            }
                        });
                    }else{
                      return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'conversation not found');
                    }
                    }else{
                        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'conversation user not found');
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    }else{
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [user report API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const user_report = async (req, res) => {
    let check = helper.checkRequest(["user_id","message"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.user_id},{},  (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                let insertData = {
                    "reported_id":req.body.user_id,
                    "report_message" : req.body.message
                };
                reportService.createData(insertData, (errorInsert, successInsert)=>{
                    if (errorInsert)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User reported successfully',successInsert);
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [user report list API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const user_report_list = async (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.user_id},{},  (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                reportService.populateData({},(errorGet, successGet)=>{
                    if (errorGet)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User reported list fetch successfully',successGet);
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [user chat clear API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const clear_chat = async (req , res) =>{
    let check = helper.checkRequest(["user_id","chat_id"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.user_id},{},  async(error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                let queryData = {_id : new ObjectId(req.body.chat_id)}
                let checkData = await userFriendService.findOneAsync(queryData,{});
                if(checkData && checkData.chat_status){
                    if(checkData.clear_chat_id == "" || (checkData.clear_chat_id == req.body.user_id)){
                       let updateData = {
                            "clear_chat_id" : new ObjectId(req.body.user_id)
                        };
                        let query = {
                            "_id":checkData._id
                        };
                        userFriendService.updateOneData(query,updateData,{ upsert: true }, async (updateError, updateSuccess) => {
                            if(updateError)
                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                            else
                                return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Conversation cleared successfully');
                        });
                    }else{
                        let updateData = {
                            $unset: {clear_chat_id: 1 },
                            "chat_status":0
                        };
                        let query = {
                            "_id":checkData._id
                        };
                        userFriendService.updateOneData(query,updateData,{ upsert: true }, async (updateError, updateSuccess) => {
                            if(updateError)
                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong',updateError);
                            else{
                                let queryChat = {
                                    "chat_id" : new ObjectId(req.body.chat_id)
                                };
                                userChatService.deleteManyRecord(queryChat, async (updateError, updateSuccess) => {
                                    if(updateError)
                                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                                    else{
                                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Conversation cleared successfully');
                                    }
                                });
                            }
                        });
                    }
                }else{
                    return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'conversation not found');
                }
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};


/**
 * [Update content]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const content_update = async (req, res) => {
    let check = helper.checkRequest(["g_content","e_content"], req.body);
    if (check == true) {
        let contentData = await contentService.findAsync({},{});
        if(contentData.length){
            let query = {_id:contentData[0]._id};
            let updateData ={
                "g_content":req.body.g_content,
                "e_content":req.body.e_content
            }
            let options = {upsert:true};

            contentService.updateOneData(query,updateData,options, (errorGet, successGet)=>{
                if (errorGet)
                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                else
                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Content updated successfully',successGet);
            });
        }else{
           return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Content npt found');
        }
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Login API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const userBlock = async (req, res) => {
    let check = helper.checkRequest(["blockedBy","blockedTo"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.blockedTo},{}, (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                let query = {
                    "_id":userInfo._id
                };
                let updateData = {
                  $push: {'blocks': req.body.blockedBy }
                };
                userService.updateOneData(query,updateData,{ upsert: true }, async (updateError, updateSuccess) => {
                    if (updateError)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else{
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User Blocked successfully');
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'This user not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Login API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const userUnBlock = async (req, res) => {
    let check = helper.checkRequest(["unBlockedBy","blockedTo"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.blockedTo},{}, (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                let query = {
                    "_id":userInfo._id
                };
                let updateData = {
                  $pull: {  blocks: req.body.blockedBy }
                };
                userService.updateOneData(query,updateData,{}, async (updateError, updateSuccess) => {
                    if (updateError)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else{
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User Unblocked successfully');
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'This user not found');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

//Get country lat lang
getLatLang = async ({country,_id}) => {
    let resultData  = await geocoder.geocode(country);
    if(resultData && resultData.length){
        let updateData = {
            "lat":resultData[0].latitude ? resultData[0].latitude : "",
            "lang":resultData[0].longitude ? resultData[0].longitude: "",
        };
        let query = {
            "_id":_id
        };
        userService.updateOneData(query,updateData,{ upsert: true }, async (updateError, updateSuccess) => {
            return updateSuccess;
        });
    }
};
/* Export apis */
module.exports = {
    login,
    signup,
    social_login,
    social_complete,
    get_latLang,
    change_password,
    delete_account,
    update_profile,
    home_user_list,
    new_user_list,
    profile_detail,
    dob_calender_event,
    date_user_list,
    send_request,
    check_request,
    request_response,
    request_user_list,
    upload_image,
    userImageUpload,
    friend_list,
    send_message,
    chat_list,
    chat_conversation,
    user_report,
    user_report_list,
    clear_chat,
    content_update,
    userBlock,
    userUnBlock
};

