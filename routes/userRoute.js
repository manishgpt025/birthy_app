// Controllers
const userController = require("../controllers/userController");
// Express router
const router = require('express').Router();
/* Routes */
router.post('/login', userController.login);
router.post('/signup', userController.signup);
router.post('/social_login', userController.social_login);
router.post('/social_complete', userController.social_complete);
router.get('/get_latLang', userController.get_latLang);
router.post('/change_password', userController.change_password);
router.post('/delete_account', userController.delete_account);
router.post('/update_profile', userController.update_profile);
router.post('/home_user_list', userController.home_user_list);
router.post('/new_user_list', userController.new_user_list);
router.post('/profile_detail', userController.profile_detail);
router.post('/dob_dates', userController.dob_calender_event);
router.post('/date_user_list', userController.date_user_list);
router.post('/send_request', userController.send_request);
router.post('/check_request', userController.check_request);
router.post('/request_user_list', userController.request_user_list);
router.post('/request_response', userController.request_response);
router.post('/upload_image', userController.upload_image);
router.post('/userImageUpload', userController.userImageUpload);
router.post('/friend_list', userController.friend_list);
router.post('/send_message', userController.send_message);
router.post('/chat_list', userController.chat_list);
router.post('/chat_conversation', userController.chat_conversation);
router.post('/user_report', userController.user_report);
router.post('/user_report_list', userController.user_report_list);
router.post('/clear_chat', userController.clear_chat);
router.post('/content_update', userController.content_update);
router.post('/userBlock', userController.userBlock);
router.post('/userUnBlock', userController.userUnBlock);

/*Export*/
module.exports = router;